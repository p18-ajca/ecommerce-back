## Retex Sprint 3

## Ce qui s'est bien passé :

    - Bonne répartition des tâches(pas de problème dans les merges).
    - Bonne Gestion du board.
    - Bon découpage des tâches: chacun a pu travailler sur des fonctionnalités.
    
## Ce qui ne s'est pas bien passer :

    - Trop de temps entre chaque merge de branche: on ne pense pas forcément à merge à la fin de chaque tâche.
    - Manque travail en pair-programming, en équipe... 
    - Manque de vision global sur le projet.

## Ce qu'on peut améliorer :

    - Faire des séances de pair-programming régulières(Surtout sur les tâches complexes).
    - Signaler la fin d'une tâche au reste de l'équipe et programmer un merge.
    - Faire des points réguliers en fin de journée afin voir ou en est le projet dans se globalité.