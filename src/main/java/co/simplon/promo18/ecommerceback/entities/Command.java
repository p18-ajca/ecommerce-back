package co.simplon.promo18.ecommerceback.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Command {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String adress;

  @NotBlank
  private String status;

  private LocalDate date;


  @OneToMany(mappedBy = "command", cascade = CascadeType.REMOVE)
  @JsonIgnore
  private List<CommandItem> commandItems = new ArrayList<CommandItem>();


  @ManyToOne
  @JsonIgnore
  private User user = new User();



  public Command() {}

  public Command(String adress, @NotBlank String status, LocalDate date) {
    this.adress = adress;
    this.status = status;
    this.date = date;
  }

  public Command(Integer id, String adress, @NotBlank String status, LocalDate date) {
    this.id = id;
    this.adress = adress;
    this.status = status;
    this.date = date;
  }

	public Command(String adress, String status, LocalDate date, User user) {
		this.adress = adress;
		this.status = status;
		this.date = date;
		this.user = user;
	}


	public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAdress() {
    return adress;
  }

  public void setAdress(String adress) {
    this.adress = adress;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public List<CommandItem> getOrderItems() {
    return commandItems;
  }

  public void setOrderItems(List<CommandItem> orderItems) {
    this.commandItems = orderItems;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
  

  
}
