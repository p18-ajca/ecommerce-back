package co.simplon.promo18.ecommerceback.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Category {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String label;

  @ManyToMany
  @JsonIgnore
  private List<Product> products = new ArrayList<Product>();

  @ManyToMany
  @JsonIgnore
  private List<User> users = new ArrayList<User>();

  public Category() {
  }

  public Category(@NotBlank String label) {
    this.label = label;
  }

  public Category(Integer id, @NotBlank String label) {
    this.id = id;
    this.label = label;
  }

	public void setProduct(Product product) {
		this.products.add(product);
	}
 
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }
}
  

  