package co.simplon.promo18.ecommerceback.entities;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

@Entity
public class News {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Integer id;

  @NotBlank
  String title;

  @Lob
  @NotBlank
  String content;
  
  String imageUrl;

  @PastOrPresent
  LocalDate date;

  public News() {}

  public News(@NotBlank String title, @NotBlank String content, String imageUrl,
      @PastOrPresent LocalDate date) {
    this.title = title;
    this.content = content;
    this.imageUrl = imageUrl;
    this.date = date;
  }

  public News(Integer id, @NotBlank String title, @NotBlank String content, String imageUrl,
      @PastOrPresent LocalDate date) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.imageUrl = imageUrl;
    this.date = date;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  


  
}
