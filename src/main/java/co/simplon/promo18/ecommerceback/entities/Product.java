package co.simplon.promo18.ecommerceback.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Product {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String title;

  private String imageUrl;

  @NotBlank
  private String author;

  @NotNull
  private int pages;

  @NotBlank
  private String editor;

  private double price;

  @ManyToMany(mappedBy = "products", cascade = CascadeType.ALL)
  @JsonIgnoreProperties({"products", "users"})
  private List<Category> categories = new ArrayList<Category>();

  @OneToMany(mappedBy = "product", cascade = CascadeType.REMOVE)
  @JsonIgnore
  private List<CommandItem> commandItems = new ArrayList<CommandItem>();

  

  public Product() {}

  public Product(@NotBlank String title, String imageUrl, @NotBlank String author,
      @NotNull int pages, @NotBlank String editor, double price) {
    this.title = title;
    this.imageUrl = imageUrl;
    this.author = author;
    this.pages = pages;
    this.editor = editor;
    this.price = price;
  }

  public Product(Integer id, @NotBlank String title, String imageUrl, @NotBlank String author,
      @NotNull int pages, @NotBlank String editor, double price) {
    this.id = id;
    this.title = title;
    this.imageUrl = imageUrl;
    this.author = author;
    this.pages = pages;
    this.editor = editor;
    this.price = price;
  }

  public void setCategory(Category category){
    this.categories.add(category);
  }


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public int getPages() {
    return pages;
  }

  public void setPages(int pages) {
    this.pages = pages;
  }

  public String getEditor() {
    return editor;
  }

  public void setEditor(String editor) {
    this.editor = editor;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public List<Category> getCategories() {
    return categories;
  }

  public void setCategories(List<Category> categories) {
    this.categories = categories;
  }


  




  
}
