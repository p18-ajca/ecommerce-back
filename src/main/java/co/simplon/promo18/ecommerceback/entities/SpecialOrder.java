package co.simplon.promo18.ecommerceback.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class SpecialOrder {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String firstName;

  @NotBlank
  private String lastName;

  @NotBlank
  private String email;

  @NotBlank
  private String phoneNumber;

  @NotBlank
  private String title;

  @NotBlank
  private String author;

  @NotBlank
  private String language;

  @NotBlank
  private String status;
  
  @ManyToOne
  @JsonIgnore
  private User user;



  public SpecialOrder() {}

  public SpecialOrder(@NotBlank String firstName, @NotBlank String lastName, @NotBlank String email,
      @NotBlank String phoneNumber, @NotBlank String title, @NotBlank String author,
      @NotBlank String language, @NotBlank String status) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.title = title;
    this.author = author;
    this.language = language;
    this.status = status;
  }

  public SpecialOrder(Integer id, @NotBlank String firstName, @NotBlank String lastName,
      @NotBlank String email, @NotBlank String phoneNumber, @NotBlank String title,
      @NotBlank String author, @NotBlank String language, @NotBlank String status) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.title = title;
    this.author = author;
    this.language = language;
    this.status = status;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
  
}
