package co.simplon.promo18.ecommerceback.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.SpecialOrder;
import co.simplon.promo18.ecommerceback.repositories.SpecialOrderRepository;

@RestController
@RequestMapping("/specialorder")
public class SpecialOrderController {


  @Autowired
  private SpecialOrderRepository repo;

  @GetMapping
  public List<SpecialOrder> getAll() {
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public SpecialOrder getOne(@PathVariable int id) {
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

  }

  @PostMapping
  public SpecialOrder add(@RequestBody @Valid SpecialOrder specialOrder) {
    specialOrder.setId(null);
    return repo.save(specialOrder);

  }

  @PutMapping("/{id}")
  public SpecialOrder update(@PathVariable int id, @Valid @RequestBody SpecialOrder specialOrder) {
    SpecialOrder toUpdate = getOne(id);
    toUpdate.setAuthor(specialOrder.getAuthor());
    toUpdate.setEmail(specialOrder.getEmail());
    toUpdate.setFirstName(specialOrder.getFirstName());
    toUpdate.setLanguage(specialOrder.getLanguage());
    toUpdate.setLastName(specialOrder.getLastName());
    toUpdate.setPhoneNumber(specialOrder.getPhoneNumber());
    toUpdate.setStatus(specialOrder.getStatus());
    toUpdate.setTitle(specialOrder.getTitle());
    return repo.save(toUpdate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delte(@PathVariable int id) {
    SpecialOrder toDelete = getOne(id);
    repo.delete(toDelete);
  }


}
