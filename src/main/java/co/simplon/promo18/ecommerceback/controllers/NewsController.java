package co.simplon.promo18.ecommerceback.controllers;

import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.repositories.NewsRepository;
import co.simplon.promo18.ecommerceback.entities.News;

@RestController
@RequestMapping("/news")
public class NewsController {
  @Autowired
  private NewsRepository repo;

  @GetMapping
  public Page<News> getAll(
    @RequestParam(required = false, defaultValue = "0") int page,
    @RequestParam(required = false, defaultValue = "5") int pageSize) {
    return repo.findAll(PageRequest.of(page, pageSize));
  }

  @GetMapping("/{id}")
  public News getOne(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)); 
  }

  @PostMapping
  public News add(@RequestBody @Valid News news){
    news.setId(null);
    return repo.save(news);

  }

  @PutMapping("/{id}")
    public News update(@PathVariable int id, @Valid @RequestBody News news){
      News toUpdate = getOne(id);
        toUpdate.setTitle(news.getTitle());
        toUpdate.setContent(news.getContent());
        toUpdate.setImageUrl(news.getImageUrl());
        toUpdate.setDate(news.getDate());
        return repo.save(toUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delte(@PathVariable int id){
      News toDelete = getOne(id);
      repo.delete(toDelete);
    }

}
