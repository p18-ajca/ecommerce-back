package co.simplon.promo18.ecommerceback.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.Command;
import co.simplon.promo18.ecommerceback.repositories.CommandRepository;


@RestController
@RequestMapping("/command")
public class CommandController {
  @Autowired
  private CommandRepository repo;

  @GetMapping
  public List<Command> getAll(){
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public Command getOne(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)); 
  }

  @GetMapping("/unique/{id}")
  public Command getUnique(@PathVariable int id){
    return repo.findByUserIdAndStatus(id, "Waiting");
  }

  @PostMapping
  public Command add(@RequestBody @Valid Command command){
    command.setId(null);
    return repo.save(command);
  }

  @PutMapping("/{id}")
  public Command update(@PathVariable int id, @Valid @RequestBody Command command){
    Command toUpdate = getOne(id);
      toUpdate.setAdress(command.getAdress());
      toUpdate.setStatus(command.getStatus());
      toUpdate.setDate(command.getDate());
      return repo.save(toUpdate);
  }

	@PutMapping("/done/{id}")
	public Command closeCommand(@PathVariable int id, @RequestBody Command command) {
		Command toUpdate = getOne(id);
		toUpdate.setStatus("Done");
		return repo.save(toUpdate);
	}

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delte(@PathVariable int id){
    Command toDelete = getOne(id);
    repo.delete(toDelete);
  }

}
