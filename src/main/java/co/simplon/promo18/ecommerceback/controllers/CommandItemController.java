package co.simplon.promo18.ecommerceback.controllers;

import co.simplon.promo18.ecommerceback.entities.Command;
import co.simplon.promo18.ecommerceback.entities.User;
import co.simplon.promo18.ecommerceback.repositories.CommandRepository;
import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.CommandItem;
import co.simplon.promo18.ecommerceback.repositories.CommandItemRepository;

@RestController
@RequestMapping("/commanditem")
public class CommandItemController {
  
  @Autowired
  private CommandItemRepository repo;

	@Autowired
	private CommandRepository commandRepository;

  @GetMapping
  public List<CommandItem> getAll(){
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public CommandItem getOne(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)); 
    
  }

  @PostMapping
  public CommandItem add(@RequestBody @Valid
			CommandItem commandItem,
			@AuthenticationPrincipal User user){
    commandItem.setId(null);
		Command command = commandRepository.findByUserIdAndStatus(user.getId(), "Waiting");

		if(command == null ) {
			LocalDate date = LocalDate.now();
			Command newCommand = new Command(
					user.getAddress(),
					"Waiting",
					date,
					user
			);
			Command createdCommand = commandRepository.save(newCommand);
			commandItem.setCommand(createdCommand);
			return repo.save(commandItem);
		}
		commandItem.setCommand(command);
    return repo.save(commandItem);
  }

  @PutMapping("/{id}")
    public CommandItem update(@PathVariable int id, @Valid @RequestBody CommandItem commandItem){
      CommandItem toUpdate = getOne(id);
        toUpdate.setPrice(commandItem.getPrice());
        toUpdate.setQuantity(commandItem.getQuantity());
        return repo.save(toUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delte(@PathVariable int id){
      CommandItem toDelete = getOne(id);
      repo.delete(toDelete);
    }

}
