package co.simplon.promo18.ecommerceback.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.Category;
import co.simplon.promo18.ecommerceback.entities.Product;
import co.simplon.promo18.ecommerceback.repositories.CategoryRepository;
import co.simplon.promo18.ecommerceback.repositories.ProductRepository;

@RestController
@RequestMapping("/product")
@Validated
public class ProductController {

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private ProductRepository repo;

  @GetMapping
  public List<Product> getAll() {
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public Product getOne(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/{id}")
  public Product add(@PathVariable int id, @RequestBody @Valid Product product){
    Category newCategory = categoryRepository.findById(id).orElseThrow();
    product.setId(null);
    product.setCategory(newCategory);
		repo.save(product);
		newCategory.setProduct(product);
		categoryRepository.save(newCategory);
		return product;
  }

  @PutMapping("/{id}")
    public Product update(@PathVariable int id, @Valid @RequestBody Product product){
      Product toUpdate = getOne(id);
        toUpdate.setImageUrl(product.getImageUrl());
        toUpdate.setTitle(product.getTitle());
        toUpdate.setAuthor(product.getAuthor());
        toUpdate.setPages(product.getPages());
        toUpdate.setEditor(product.getEditor());
        toUpdate.setPrice(product.getPrice());
        return repo.save(toUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delte(@PathVariable int id){
      Product toDelete = getOne(id);
      repo.delete(toDelete);
    }


  
}
