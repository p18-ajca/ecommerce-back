package co.simplon.promo18.ecommerceback.controllers;

import co.simplon.promo18.ecommerceback.entities.Product;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.Category;
import co.simplon.promo18.ecommerceback.entities.Product;
import co.simplon.promo18.ecommerceback.repositories.CategoryRepository;



@RestController
@RequestMapping("/category")
public class CategoryController {

  @Autowired
  private CategoryRepository repo;

  @GetMapping
  public List<Category> getAll(){
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public Category getOne(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)); 
  }

  @GetMapping("/{id}/product")
  public List<Product> getOneProduct(@PathVariable int id){
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)).getProducts(); 
  }

  @PostMapping
  public Category add(@RequestBody @Valid Category category){
    category.setId(null);
    return repo.save(category);
  }

  @PutMapping("/{id}")
  public Category update(@PathVariable int id, @Valid @RequestBody Category category){
    Category toUpdate = getOne(id);
      toUpdate.setLabel(category.getLabel());
      return repo.save(toUpdate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delte(@PathVariable int id){
    Category toDelete = getOne(id);
    repo.delete(toDelete);
  }

  /* TODO:
  @PostMapping("/{categoryId}/product/{productId}")
  public List<Product> addProductToCategory(@PathVariable int categoryId, @PathVariable int productId){
    //TODO: return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)).getProducts();
  }
*/

}
