package co.simplon.promo18.ecommerceback.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.ecommerceback.entities.User;
import co.simplon.promo18.ecommerceback.repositories.UserRepository;



@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
  private UserRepository repo;

  @Autowired
  private PasswordEncoder encoder;

  @GetMapping
  public List<User> getAll() {
    return repo.findAll();
  }

  @GetMapping("/account")
  public User getAccount(@AuthenticationPrincipal User user) {
    return user;
  }

  @GetMapping("/{id}")
  public User getOne(@PathVariable int id) {
    return repo.findById(id)
      .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

  }

  @PostMapping
  public User add(@RequestBody @Valid User user) {
    if(repo.findByEmail(user.getEmail()).isPresent())
    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
    user.setId(null);
    user.setRole("ROLE_USER");
    String hashed = encoder.encode(user.getPassword());
    user.setPassword(hashed);
    repo.save(user);
    return user;
  }

  @PutMapping("/{id}")
  public User update(@PathVariable int id, @Valid @RequestBody User user) {
    User toUpdate = getOne(id);
    toUpdate.setAddress(user.getAddress());
    toUpdate.setCity(user.getCity());
    toUpdate.setDate(user.getDate());
    toUpdate.setFirstName(user.getFirstName());
    toUpdate.setGender(user.getGender());
    toUpdate.setLastName(user.getLastName());
    toUpdate.setPassword(user.getPassword());
    toUpdate.setPhoneNumber(user.getPhoneNumber());
    toUpdate.setRole(user.getRole());
    toUpdate.setZipCode(user.getZipCode());
    return repo.save(toUpdate);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delte(@PathVariable int id) {
    User toDelete = getOne(id);
    repo.delete(toDelete);
  }


}

