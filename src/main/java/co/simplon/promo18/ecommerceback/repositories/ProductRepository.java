package co.simplon.promo18.ecommerceback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
  
}
