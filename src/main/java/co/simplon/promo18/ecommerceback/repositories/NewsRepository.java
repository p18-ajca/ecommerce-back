package co.simplon.promo18.ecommerceback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.News;

@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {
  
}
