package co.simplon.promo18.ecommerceback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.CommandItem;


@Repository
public interface CommandItemRepository extends JpaRepository<CommandItem, Integer> {
  
}
