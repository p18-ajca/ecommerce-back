package co.simplon.promo18.ecommerceback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.ecommerceback.entities.Command;


@Repository
public interface CommandRepository extends JpaRepository<Command, Integer> {
  Command findByUserIdAndStatus(int userId, String status);

}


