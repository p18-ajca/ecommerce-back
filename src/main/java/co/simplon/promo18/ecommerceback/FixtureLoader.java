package co.simplon.promo18.ecommerceback;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import co.simplon.promo18.ecommerceback.entities.Category;
import co.simplon.promo18.ecommerceback.entities.Command;
import co.simplon.promo18.ecommerceback.entities.CommandItem;
import co.simplon.promo18.ecommerceback.entities.News;
import co.simplon.promo18.ecommerceback.entities.Product;
import co.simplon.promo18.ecommerceback.entities.SpecialOrder;
import co.simplon.promo18.ecommerceback.entities.User;

@Component
public class FixtureLoader {
  @PersistenceContext
  private EntityManager em;

  @Autowired
  PasswordEncoder encoder;

  @EventListener(ApplicationReadyEvent.class)
  @Transactional
  public void load() {



    List<String> categories = Arrays.asList(new String[] {"Horreur", "Romance", "Policier", "Historique", "Philosophie", "Science-fiction", "Drame"});
    List<User> users = new ArrayList<User>();
    List<Product> products = new ArrayList<Product>();

    Random rand = new Random();
    Faker faker = new Faker();
    // User de Login

    // News
    for (int i = 1; i <= 11; i++) {
      News news = new News(
        faker.book().title(),
        faker.lorem().paragraph(),
        "./assets/Static/img" + i + ".jpg",
        faker.date().past(10, TimeUnit.DAYS)
          .toInstant()
          .atZone(ZoneId.systemDefault())
          .toLocalDate()
        );
      em.persist(news);
    }
    // Products
    for (int i = 0; i < faker.number().numberBetween(32, 64); i++) {
      Product product = new Product(
        faker.book().title(),
        "/img/lala/youyou",
        faker.book().author(),
        (int)(Math.random() * 600 - 100 + 1) + 100,
        faker.book().publisher(),
        faker.number().randomDouble(2, 1, 50)
      );
      products.add(product);
      em.persist(product);
    }
    //User
    for (int i = 0; i < faker.number().numberBetween(11, 23); i++) {
      if (i == 0) {
        User user = new User(
          "Test",
          "Panda",
          "test@gmail.com",
          encoder.encode("1234"),
          faker.demographic().sex(),
          faker.date().past(20000, TimeUnit.DAYS)
            .toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate(),
          faker.address().streetAddress(),
          faker.address().city(),
          faker.address().zipCode(),
          "789379938",
          "ROLE_ADMIN"
        );
        for (int z = 0; z < faker.number().numberBetween(2, 5); z++) {
          SpecialOrder specialOrder = new SpecialOrder(
            user.getFirstName(),
            user.getLastName(),
            user.getEmail(),
            user.getPhoneNumber(),
            faker.book().title(),
            faker.book().author(),
            "French",
            "Waiting"
          );
          specialOrder.setUser(user);
          em.persist(specialOrder);
          }
          // Commands
            Command command = new Command(
              user.getAddress(),
              "Waiting",
              faker.date().future(20000, TimeUnit.DAYS)
              .toInstant()
              .atZone(ZoneId.systemDefault())
              .toLocalDate()
            );
        for (int z = 0; z < faker.number().numberBetween(1, 3); z++) {
          Command commandDone = new Command(
            user.getAddress(),
            "Done",
            faker.date().future(20000, TimeUnit.DAYS)
            .toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate()
          );
        commandDone.setUser(user);
        em.persist(commandDone);
        command.setUser(user);
        em.persist(command);
        //CommandItems
        for (int x = 0; x <= faker.number().numberBetween(0, 6); x++) {
          CommandItem item = new CommandItem(
            faker.number().randomDouble(2, 1, 1000),
            faker.number().numberBetween(1, 10)
          );
          item.setCommand(command);
          item.setProduct(products.get(
            faker.number()
            .numberBetween(0, products.size() - 1))
          );
          em.persist(item);
        }
      }
        em.persist(user);
        users.add(user);
      }
      User user = new User(
        faker.name().firstName(),
        faker.name().lastName(),
        faker.internet().emailAddress(),
        encoder.encode(faker.internet().password()),
        faker.demographic().sex(),
        faker.date().past(20000, TimeUnit.DAYS)
          .toInstant()
          .atZone(ZoneId.systemDefault())
          .toLocalDate(),
        faker.address().streetAddress(),
        faker.address().city(),
        faker.address().zipCode(),
        "789379938",
        "ROLE_USER"
      );
      // SpecialOrders
      for (int z = 0; z < faker.number().numberBetween(2, 5); z++) {
        SpecialOrder specialOrder = new SpecialOrder(
          user.getFirstName(),
          user.getLastName(),
          user.getEmail(),
          user.getPhoneNumber(),
          faker.book().title(),
          faker.book().author(),
          "French",
          "Waiting"
        );
        specialOrder.setUser(user);
        em.persist(specialOrder);
        }
        // Commands
          Command command = new Command(
            user.getAddress(),
            "Waiting",
            faker.date().future(20000, TimeUnit.DAYS)
            .toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate()
          );
          for (int z = 0; z < faker.number().numberBetween(1, 3); z++) {
            Command commandDone = new Command(
              user.getAddress(),
              "Done",
              faker.date().future(20000, TimeUnit.DAYS)
              .toInstant()
              .atZone(ZoneId.systemDefault())
              .toLocalDate()
            );
          commandDone.setUser(user);
          em.persist(commandDone);
          command.setUser(user);
          em.persist(command);
          //CommandItems
          for (int x = 0; x <= faker.number().numberBetween(0, 6); x++) {
            CommandItem item = new CommandItem(
              faker.number().randomDouble(2, 1, 1000),
              faker.number().numberBetween(1, 10)
            );
            item.setCommand(command);
            item.setProduct(products.get(
              faker.number()
              .numberBetween(0, products.size() - 1))
            );
            em.persist(item);
          }
        }
        em.persist(user);
        users.add(user);
      }
    //Category
    for (String c : categories) {
      int randUsers = rand.nextInt(users.size() - 3);
      int randProducts = rand.nextInt(products.size() - 3);
      List<User> usersForCat = Arrays.asList(new User[] { 
        users.get(randUsers),
        users.get(randUsers + 1),
        users.get(randUsers + 3),
      });
      List<Product> productsForCat = Arrays.asList(new Product[] {
        products.get(randProducts),
        products.get(randProducts + 1),
        products.get(randProducts + 3),
      });
      Category category = new Category(c);
      em.persist(category);
      category.setProducts(productsForCat);
      category.setUsers(usersForCat);
    }

    }
}

