package co.simplon.promo18.ecommerceback.Authentication;

import org.hibernate.validator.internal.metadata.core.AnnotationProcessingOptionsImpl.ExecutableParameterKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;


@Configuration
public class SecurityConfig {

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.httpBasic();
    http.cors().configurationSource(request -> corsConfiguration());
    http.authorizeRequests()
      .anyRequest().permitAll()
      .and().csrf().disable()
      .logout().logoutSuccessHandler((req, res, auth) -> {
        res.setStatus(204);
      });
    return http.build();
  }


  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(12);
  }

  private CorsConfiguration corsConfiguration() {
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin("http://localhost:4200");
    config.addAllowedHeader("*");
    config.addAllowedMethod("*");
    return config;
  }
  
}
