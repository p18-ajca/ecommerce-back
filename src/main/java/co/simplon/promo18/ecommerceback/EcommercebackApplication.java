package co.simplon.promo18.ecommerceback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommercebackApplication {

  public static void main(String[] args) {
    SpringApplication.run(EcommercebackApplication.class, args);
  }
}
